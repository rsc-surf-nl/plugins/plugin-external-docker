#!/bin/bash
set -x

# Get list of local users with UID > 1000 and filter out 'nobody'
active_users=$(awk -F: '$3 > 1000 && $1 != "nobody" { print $1 }' /etc/passwd)

for src_user in $active_users; do
    # Check if Docker is already set up for this user, ignore error if not installed
    sudo -u "$src_user" bash -c 'PATH=$HOME/bin:$PATH docker --version'

    if [ $? -eq 0 ]; then
        echo "Docker already set up for $src_user, skipping..."
        continue
    fi

    # Enable linger for the user to keep Docker running after logout
    loginctl enable-linger "$src_user"

    sudo -u "$src_user" bash -c '
        export PATH=$HOME/bin:$PATH
        export XDG_RUNTIME_DIR=/run/user/$(id -u)
        export DOCKER_HOST=unix://$XDG_RUNTIME_DIR/docker.sock

        # Download and run the rootless Docker installation script
        curl -fsSL https://get.docker.com/rootless -o ~/get-docker.sh && sh ~/get-docker.sh

        # Enable linger for the user to keep Docker running after logout
        loginctl enable-linger $(whoami)

        # Add Docker environment variables to .bashrc for future sessions
        echo "export PATH=\$HOME/bin:\$PATH" >> ~/.bashrc
        echo "export DOCKER_HOST=unix://\$XDG_RUNTIME_DIR/docker.sock" >> ~/.bashrc

        # Initialize rootless Docker for the user
        dockerd-rootless-setuptool.sh install

        # Start and enable Docker rootless service
        systemctl --user start docker.service
        systemctl --user enable docker.service

        # Verify Docker installation
        docker ps && echo "Docker set up successfully for $(whoami)"
    '
done
